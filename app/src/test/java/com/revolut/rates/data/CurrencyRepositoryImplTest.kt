package com.revolut.rates.data

import android.content.Context
import android.content.res.Resources
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.AdditionalMatchers
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class CurrencyRepositoryImplTest {

    @Mock
    lateinit var mockContext: Context
    @Mock
    lateinit var mockResources: Resources

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getFlagSrc_correct() = runBlockingTest {
        val packageName = "com.revolut.rates"
        val currency = "USD"
        val expected = 123
        `when`(mockContext.resources).thenReturn(mockResources)
        `when`(mockContext.packageName).thenReturn(packageName)
        `when`(mockResources.getIdentifier(anyString(), anyString(), anyString())).thenReturn(expected)

        val repository = CurrencyRepositoryImpl(mockContext)
        val actual = repository.getFlagSrc(currency)

        Assert.assertEquals(expected, actual)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getFlagSrc_correctIdentifier() = runBlockingTest {
        val packageName = "com.revolut.rates"
        val currency = "USD"
        `when`(mockContext.resources).thenReturn(mockResources)
        `when`(mockContext.packageName).thenReturn(packageName)
        `when`(mockContext.getString(anyInt())).thenReturn("")
        `when`(mockResources.getIdentifier(anyString(), anyString(), anyString())).thenReturn(0)

        val repository = CurrencyRepositoryImpl(mockContext)
        repository.getFlagSrc(currency)

        verify(mockResources, times(1)).getIdentifier(
            startsWith("ic_"), eq("drawable"), eq(packageName)
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getFlagSrc_correctCaching() = runBlockingTest {
        val packageName = "com.revolut.rates"
        val currency = "usd"
        val currency2 = "aud"
        val currency3 = "cad"
        `when`(mockContext.resources).thenReturn(mockResources)
        `when`(mockContext.packageName).thenReturn(packageName)
        `when`(mockResources.getIdentifier(anyString(), anyString(), anyString())).thenReturn(0)

        val repository = CurrencyRepositoryImpl(mockContext)
        repository.getFlagSrc(currency)
        repository.getFlagSrc(currency2)
        repository.getFlagSrc(currency2)
        repository.getFlagSrc(currency3)
        repository.getFlagSrc(currency3)
        repository.getFlagSrc(currency3)

        verify(mockResources, times(1)).getIdentifier(
            eq("ic_$currency"), eq("drawable"), eq(packageName)
        )
        verify(mockResources, times(1)).getIdentifier(
            eq("ic_$currency2"), eq("drawable"), eq(packageName)
        )
        verify(mockResources, times(1)).getIdentifier(
            eq("ic_$currency3"), eq("drawable"), eq(packageName)
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getDescription_correct() = runBlockingTest {
        val packageName = "com.revolut.rates"
        val currency = "USD"
        val expected = "US Dollar"
        val resId = 123
        `when`(mockContext.resources).thenReturn(mockResources)
        `when`(mockContext.packageName).thenReturn(packageName)
        `when`(mockResources.getIdentifier(anyString(), anyString(), anyString())).thenReturn(resId)
        `when`(mockContext.getString(eq(resId))).thenReturn(expected)
        `when`(mockContext.getString(AdditionalMatchers.not(eq(resId)))).thenReturn("")

        val repository = CurrencyRepositoryImpl(mockContext)
        val actual = repository.getDescription(currency)

        Assert.assertEquals(expected, actual)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getDescription_correctIdentifier() = runBlockingTest {
        val packageName = "com.revolut.rates"
        val currency = "USD"
        val currency2 = "AUD"
        val currency3 = "CAD"
        `when`(mockContext.resources).thenReturn(mockResources)
        `when`(mockContext.packageName).thenReturn(packageName)
        `when`(mockContext.getString(anyInt())).thenReturn("")
        `when`(mockResources.getIdentifier(anyString(), anyString(), anyString())).thenReturn(0)

        val repository = CurrencyRepositoryImpl(mockContext)
        repository.getDescription(currency)
        repository.getDescription(currency2)
        repository.getDescription(currency2)
        repository.getDescription(currency3)
        repository.getDescription(currency3)
        repository.getDescription(currency3)

        verify(mockResources, times(1)).getIdentifier(
            eq(currency), eq("string"), eq(packageName)
        )
        verify(mockResources, times(1)).getIdentifier(
            eq(currency2), eq("string"), eq(packageName)
        )
        verify(mockResources, times(1)).getIdentifier(
            eq(currency3), eq("string"), eq(packageName)
        )
    }
}