package com.revolut.rates.ui.model

import com.revolut.rates.R

object DummyItems {
    val currencies = listOf(
        CurrencyItem("USD", "USD", "1234.3", "US Dollar", R.drawable.ic_usd),
        CurrencyItem("EUR", "EUR", "2345.6", "Euro", R.drawable.ic_eur),
        CurrencyItem("SEK", "SEK", "3456.9", "Swedish Krona", R.drawable.ic_sek),
        CurrencyItem("CAD", "CAD", "4567.2", "Canadian Dollar", R.drawable.ic_cad)
    )
}