package com.revolut.rates.data

import android.content.Context
import com.revolut.rates.R
import com.revolut.rates.domain.CurrencyRepository
import java.util.*

/**
 *  Local currency presentation data repository.
 *  All icons are downloaded from [https://github.com/transferwise/currency-flags],
 *  renamed with added suffix 'ic_', and imported into the project.
 */
class CurrencyRepositoryImpl(private val context: Context) : CurrencyRepository {

    private val iconCache = mutableMapOf<String, Int>()
    private val descriptionCache = mutableMapOf<String, String>()

    override suspend fun getFlagSrc(name: String): Int = iconCache[name] ?: run {
        context.resources.getIdentifier(
            "ic_${name.toLowerCase(Locale.getDefault())}", "drawable", context.packageName
        ).apply {
            iconCache[name] = if (this > 0) this else R.drawable.ic_flag_placeholder
        }
    }

    override suspend fun getDescription(name: String): String =
        descriptionCache[name] ?: run {
            val resId = context.resources.getIdentifier(name, "string", context.packageName)
            val description = if (resId > 0) context.getString(resId) else ""
            descriptionCache[name] = description

            description
        }
}