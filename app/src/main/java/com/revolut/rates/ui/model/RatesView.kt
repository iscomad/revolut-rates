package com.revolut.rates.ui.model

data class RatesView(
    val baseCurrencyItem: CurrencyItem,
    val result: List<CurrencyItem>
)