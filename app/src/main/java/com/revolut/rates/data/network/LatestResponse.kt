package com.revolut.rates.data.network

import java.util.*

data class LatestResponse(
    val base: String,
    val date: Date,
    val rates: Map<String, Double>
)
