package com.revolut.rates.ui

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.revolut.rates.R
import com.revolut.rates.ui.model.CurrencyItem
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private val ratesViewModel: RatesViewModel by viewModel()
    private lateinit var currenciesAdapter: CurrenciesAdapter
    private lateinit var timer: Timer
    private lateinit var layoutManager: LinearLayoutManager

    private var baseCurrencyItem: CurrencyItem = CURRENCY_ITEM_USD

    private val updateTask = object : TimerTask() {
        override fun run() {
            ratesViewModel.loadRates(baseCurrencyItem)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        baseCurrencyItem = baseCurrencyItem.copy(description = getString(R.string.USD))

        currenciesAdapter = CurrenciesAdapter().apply {
            onBaseSelected = {
                baseCurrencyItem = it
                recyclerView.scrollToPosition(0)
                showKeyboardDelayed()
            }
            onBaseAmountChanged = { baseCurrencyItem = baseCurrencyItem.copy(amount = it) }
        }
        recyclerView.adapter = currenciesAdapter
        layoutManager = recyclerView.layoutManager as? LinearLayoutManager
            ?: throw IllegalStateException("The recyclerView's layoutManager expected to be LinearLayoutManager")

        ratesViewModel.ratesLiveData.observe(this, Observer {
            if (baseCurrencyItem == it.baseCurrencyItem) {
                currenciesAdapter.updateData(it.result)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        timer = Timer()
        timer.scheduleAtFixedRate(updateTask, 0L, 1000L)
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }

    private fun showKeyboardDelayed() {
        val imm = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getSystemService(InputMethodManager::class.java)
        } else {
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        }

        recyclerView.postDelayed({
            if (layoutManager.findFirstVisibleItemPosition() == 0) {
                layoutManager.findViewByPosition(0)?.findViewById<EditText>(R.id.amountView)?.let {
                    it.requestFocus()
                    imm.showSoftInput(it, InputMethodManager.SHOW_IMPLICIT)
                }
            }
        }, 300L)
    }

    companion object {
        val CURRENCY_ITEM_USD = CurrencyItem("USD", "USD", "1", "", R.drawable.ic_usd)
    }
}
