package com.revolut.rates.ui.model

sealed class CurrencyItemPayload {
    object AmountChanged : CurrencyItemPayload()
}