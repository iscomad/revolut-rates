package com.revolut.rates

import android.app.Application
import com.revolut.rates.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RatesApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@RatesApplication)
            modules(appModule)
        }
    }
}