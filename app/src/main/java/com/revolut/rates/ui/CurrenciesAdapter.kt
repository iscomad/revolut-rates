package com.revolut.rates.ui

import android.os.Handler
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.revolut.rates.ui.model.CurrencyItem
import com.revolut.rates.ui.model.CurrencyItemPayload
import kotlinx.android.synthetic.main.item_currency.view.*

class CurrenciesAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    var onBaseSelected: ((item: CurrencyItem) -> Unit)? = null
    var onBaseAmountChanged: ((String) -> Unit)? = null

    private val handler = Handler()

    private var items: List<CurrencyItem> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder =
        if (viewType == ViewType.BASE.ordinal) {
            BaseCurrencyViewHolder(parent)
        } else {
            CurrencyViewHolder(parent)
        }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) ViewType.BASE.ordinal else ViewType.NORMAL.ordinal
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val currencyItem = items[position]
        holder.bind(currencyItem)
        if (holder is BaseCurrencyViewHolder) {
            holder.itemView.amountView.doAfterTextChanged {
                handler.post {
                    onBaseAmountChanged?.invoke(it.toString())
                }
            }
        } else {
            holder.itemView.setOnClickListener {
                handler.post {
                    onBaseSelected?.invoke(currencyItem)
                    setBase(currencyItem)
                }
            }
        }
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder, position: Int, payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            if (payloads.any { it is CurrencyItemPayload.AmountChanged } && position > 0) {
                holder.itemView.amountView.setText("${items[position].amount}")
            }
        }
    }

    fun updateData(data: List<CurrencyItem>) {
        val diffCallback = DiffCallback(items, data)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items = data
        diffResult.dispatchUpdatesTo(this)
    }

    private fun setBase(item: CurrencyItem) {
        val position = items.indexOfFirst { it.id == item.id }
        items = items.toMutableList().apply {
            removeAt(position)
            add(0, item)
        }
        notifyItemMoved(position, 0)
    }

    class DiffCallback(
        private val oldList: List<CurrencyItem>, private val newList: List<CurrencyItem>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return oldList[oldPosition] == newList[newPosition]
        }

        @Nullable
        override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
            val oldItem = oldList[oldPosition]
            val newItem = newList[newPosition]
            if (oldItem.amount != newItem.amount) {
                return CurrencyItemPayload.AmountChanged
            }

            return null
        }
    }

    enum class ViewType {
        BASE, NORMAL
    }
}
