package com.revolut.rates.domain.model

data class Rate(
    val to: String,
    val amount: Double
)
