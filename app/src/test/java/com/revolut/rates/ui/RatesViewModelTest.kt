package com.revolut.rates.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.revolut.rates.domain.CurrencyRepository
import com.revolut.rates.domain.RatesRepository
import com.revolut.rates.domain.Result
import com.revolut.rates.domain.model.Currency
import com.revolut.rates.domain.model.Rate
import com.revolut.rates.ui.model.CurrencyItem
import com.revolut.rates.utils.MainCoroutineRule
import com.revolut.rates.utils.getOrAwaitValue
import com.revolut.rates.utils.runBlocking
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class RatesViewModelTest {

    lateinit var ratesViewModel: RatesViewModel

    @Mock
    lateinit var ratesRepository: RatesRepository
    @Mock
    lateinit var currencyRepository: CurrencyRepository

    private val currencies = listOf(
        Currency("USD", "", Rate("AUD", 0.9)),
        Currency("GBP", "", Rate("AUD", 0.7))
    )
    private val baseCurrencyItem = CurrencyItem("AUD", "", "1.0", "", 0)

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutineRule = MainCoroutineRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        ratesViewModel = RatesViewModel(ratesRepository, currencyRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testLoadRates_success() = coroutineRule.runBlocking {
        `when`(ratesRepository.getLatestRates(baseCurrencyItem.id))
            .thenReturn(Result.Success(currencies))
        `when`(currencyRepository.getDescription(ArgumentMatchers.anyString())).thenReturn("")
        `when`(currencyRepository.getFlagSrc(ArgumentMatchers.anyString())).thenReturn(0)

        ratesViewModel.loadRates(baseCurrencyItem)

        val ratesView = ratesViewModel.ratesLiveData.getOrAwaitValue()
        Assert.assertEquals(baseCurrencyItem, ratesView.baseCurrencyItem)
        Assert.assertEquals(currencies.size + 1, ratesView.result.size)
    }
}