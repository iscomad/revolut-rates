package com.revolut.rates.domain

interface CurrencyRepository {
    suspend fun getFlagSrc(name: String): Int
    suspend fun getDescription(name: String): String
}