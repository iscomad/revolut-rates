package com.revolut.rates.ui.model

data class CurrencyItem(
    val id: String,
    val title: CharSequence,
    val amount: CharSequence,
    val description: CharSequence,
    val icon: Int
)
