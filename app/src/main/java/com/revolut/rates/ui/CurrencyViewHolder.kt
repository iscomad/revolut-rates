package com.revolut.rates.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.revolut.rates.R
import com.revolut.rates.ui.model.CurrencyItem
import kotlinx.android.synthetic.main.item_currency.view.*

class CurrencyViewHolder(parent: ViewGroup) : BaseViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
) {

    override fun bind(item: CurrencyItem): Unit = itemView.apply {
        iconView.setImageResource(item.icon)
        titleView.text = item.title
        descriptionView.text = item.description
        amountView.setText(item.amount)
        amountView.isFocusable = false
    }.run {}
}
