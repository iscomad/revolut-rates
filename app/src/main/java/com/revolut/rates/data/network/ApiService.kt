package com.revolut.rates.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/latest")
    suspend fun latestRates(@Query("base") base: String): Response<LatestResponse>
}