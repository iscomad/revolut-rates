package com.revolut.rates.data

import com.revolut.rates.data.network.ApiService
import com.revolut.rates.data.network.LatestResponse
import com.revolut.rates.domain.Result
import com.revolut.rates.domain.model.Currency
import com.revolut.rates.domain.model.Rate
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import retrofit2.Response
import java.util.*

class RatesRepositoryImplTest {

    @Mock
    lateinit var apiService: ApiService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getLatestRates_success() = runBlockingTest {
        val baseCurrency = "USD"
        val response = Response.success(LatestResponse(baseCurrency, Date(), emptyMap()))
        `when`(apiService.latestRates(baseCurrency)).thenReturn(response)

        val repository = RatesRepositoryImpl(apiService)
        val actual = repository.getLatestRates(baseCurrency)

        assertTrue(actual is Result.Success)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getLatestRates_correctList() = runBlockingTest {
        val baseCurrency = "USD"
        val ratesMap = mapOf("AUD" to 2.7, "CAD" to 1.1)
        val expected = listOf(
            Currency("AUD", "", Rate(baseCurrency, 2.7)),
            Currency("CAD", "", Rate(baseCurrency, 1.1))
        )
        val response = Response.success(LatestResponse(baseCurrency, Date(), ratesMap))
        `when`(apiService.latestRates(baseCurrency)).thenReturn(response)

        val repository = RatesRepositoryImpl(apiService)
        val actual = (repository.getLatestRates(baseCurrency) as? Result.Success)?.data

        assertEquals(expected, actual)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getLatestRates_error() = runBlockingTest {
        val baseCurrency = "USD"
        val response = Response.error<LatestResponse>(
            403,
            "{\"error\":[\"Something went wrong\"]}".toResponseBody("application/json".toMediaTypeOrNull())
        )
        `when`(apiService.latestRates(baseCurrency)).thenReturn(response)

        val repository = RatesRepositoryImpl(apiService)
        val actual = repository.getLatestRates(baseCurrency)

        assertTrue(actual is Result.Error)
    }
}