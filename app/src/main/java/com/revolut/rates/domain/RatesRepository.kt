package com.revolut.rates.domain

import com.revolut.rates.domain.model.Currency

interface RatesRepository {
    suspend fun getLatestRates(base: String): Result<List<Currency>>
}