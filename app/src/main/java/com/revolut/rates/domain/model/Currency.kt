package com.revolut.rates.domain.model

data class Currency(
    val id: String,
    val description: String,
    val rate: Rate
)
