package com.revolut.rates.di

import com.google.gson.GsonBuilder
import com.revolut.rates.data.CurrencyRepositoryImpl
import com.revolut.rates.data.RatesRepositoryImpl
import com.revolut.rates.data.network.ApiService
import com.revolut.rates.domain.CurrencyRepository
import com.revolut.rates.domain.RatesRepository
import com.revolut.rates.ui.RatesViewModel
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier

val appModule = module {

    single { provideApiService() }
    single<RatesRepository> { RatesRepositoryImpl(get()) }
    single<CurrencyRepository> { CurrencyRepositoryImpl(get()) }

    viewModel { RatesViewModel(get(), get()) }
}

private fun provideApiService(): ApiService {
    val gson = GsonBuilder()
        .setDateFormat(DATE_FORMAT_PATTERN)
        .create()
    val httpClient = OkHttpClient.Builder()
        .hostnameVerifier(HostnameVerifier { _, _ -> true })
        .connectTimeout(1, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .build()
    val retrofit = Retrofit.Builder()
        .baseUrl("https://revolut.duckdns.org")
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    return retrofit.create(ApiService::class.java)
}

private const val DATE_FORMAT_PATTERN = "yyyy-MM-dd"
