package com.revolut.rates.data

import com.revolut.rates.data.network.ApiService
import com.revolut.rates.domain.RatesRepository
import com.revolut.rates.domain.Result
import com.revolut.rates.domain.model.Currency
import com.revolut.rates.domain.model.Rate

class RatesRepositoryImpl(private val apiService: ApiService) : RatesRepository {

    override suspend fun getLatestRates(base: String): Result<List<Currency>> {
        val response = apiService.latestRates(base)
        return if (response.isSuccessful) {
            val currencies = response.body()?.rates?.map {
                Currency(it.key, "", Rate(base, it.value))
            }
            currencies?.let { Result.Success(it) } ?: Result.Error("Parse error")
        } else {
            Result.Error(response.errorBody()?.toString() ?: "Unknown error")
        }
    }
}