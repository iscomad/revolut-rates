package com.revolut.rates.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.revolut.rates.ui.model.CurrencyItem

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(item: CurrencyItem)
}