package com.revolut.rates.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.revolut.rates.domain.CurrencyRepository
import com.revolut.rates.domain.RatesRepository
import com.revolut.rates.domain.Result
import com.revolut.rates.domain.model.Currency
import com.revolut.rates.ui.model.CurrencyItem
import com.revolut.rates.ui.model.RatesView
import kotlinx.coroutines.launch

class RatesViewModel(
    private val ratesRepository: RatesRepository,
    private val currencyRepository: CurrencyRepository
) : ViewModel() {

    private val _ratesLiveData = MutableLiveData<RatesView>()
    val ratesLiveData: LiveData<RatesView> = _ratesLiveData

    fun loadRates(baseCurrencyItem: CurrencyItem) = viewModelScope.launch {
        when (val result = ratesRepository.getLatestRates(baseCurrencyItem.id)) {
            is Result.Success -> {
                val amount = baseCurrencyItem.amount.toString().toDoubleOrNull() ?: .0
                _ratesLiveData.value = RatesView(
                    baseCurrencyItem,
                    result.data.map {
                        mapCurrency(it, amount)
                    }.sortedBy { it.id }.toMutableList().apply { add(0, baseCurrencyItem) }
                )
            }
            is Result.Error ->
                Log.e(TAG, "Error loading rates for base ${baseCurrencyItem.id}: ${result.error}")
        }
    }

    private suspend fun mapCurrency(currency: Currency, baseAmount: Double): CurrencyItem {
        val key = currency.id
        return CurrencyItem(
            key, key, (currency.rate.amount * baseAmount).toString(),
            currencyRepository.getDescription(key), currencyRepository.getFlagSrc(key)
        )
    }
}